// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({ code: validation.code });

type Props = { navigation: NavigationScreenProp<*> };
class ConfirmEmail extends React.Component<Props> {
	onSubmitHandler = ({ code }: { code: string }) => {
		console.log('code', code);
		this.props.navigation.navigate('Workspace');
	};
	updateEmailHandler = () => {};
	resendCodeHandler = () => {};
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Please check your email
				</Text>
				<Text style={[human.body, { paddingBottom: 16 }]}>
					I&apos;ve sent you a 6-digit confirmation code to your email address.
					It will expire shortly, so enter your confirmation code as soon as
					posible.
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ code: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									label="Your confirmation code"
									placeholder="Confirmation code"
									containerStyle={commonStyles.inputContainer}
									name="code"
									onChangeText={setFieldValue}
									value={values.code}
									onBlur={setFieldTouched}
									errorMessage={errors.code && touched.code ? errors.code : ''}
									keyboardType="numeric"
								/>
								<Button
									title="Confirm email"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
				<View>
					<Text
						style={[
							human.subhead,
							{ color: iOSColors.gray, paddingVertical: 5 },
						]}
					>
						Keep this window open while checking for your code. Haven&apos;t our
						email? Check your email. You still didn&apos;t receive the code?
					</Text>
					<Button
						title="Resend code."
						titleStyle={[human.subhead, commonStyles.footnoteText]}
						clear
						onPress={this.resendCodeHandler}
					/>
				</View>
				<View>
					<Text
						style={[
							human.footnote,
							{ color: iOSColors.gray, paddingVertical: 5 },
						]}
					>
						Wrong email? Please
					</Text>
					<Button
						title="re-enter your email address."
						titleStyle={[human.footnote, commonStyles.footnoteText]}
						clear
						onPress={this.updateEmailHandler}
					/>
				</View>
			</View>
		);
	}
}

export default ConfirmEmail;
