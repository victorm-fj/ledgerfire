// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import { human } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	email: validation.email,
	accountantEmail: validation.email,
	advisorEmail: validation.email,
});

type Props = { navigation: NavigationScreenProp<*> };
class Invite extends React.Component<Props> {
	onSubmitHandler = () => {
		this.props.navigation.navigate('Dashboard');
	};
	inviteHandler = () => {};
	skipHandler = () => {
		this.props.navigation.navigate('Dashboard');
	};
	render() {
		return (
			<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
				<View style={commonStyles.screenContainer}>
					<Text style={[human.title2, { paddingBottom: 24 }]}>
						Invite your business team
					</Text>
					<KeyboardAvoidingView
						style={{ flex: 1 }}
						keyboardVerticalOffset={64}
						behavior={platformBehavior}
					>
						<Formik
							initialValues={{
								email: '',
								accountantEmail: '',
								advisorEmail: '',
							}}
							validationSchema={validationSchema}
							onSubmit={this.onSubmitHandler}
							render={({
								setFieldValue,
								values,
								setFieldTouched,
								errors,
								touched,
								handleSubmit,
							}) => (
								<View style={commonStyles.formContainer}>
									<FormikInput
										label="Email address"
										placeholder="name@example.com"
										containerStyle={commonStyles.inputContainer}
										name="email"
										onChangeText={setFieldValue}
										value={values.email}
										onBlur={setFieldTouched}
										errorMessage={
											errors.email && touched.email ? errors.email : ''
										}
										keyboardType="email-address"
									/>
									<FormikInput
										label="Invite your accountant"
										placeholder="name@example.com"
										containerStyle={commonStyles.inputContainer}
										name="accountantEmail"
										onChangeText={setFieldValue}
										value={values.accountantEmail}
										onBlur={setFieldTouched}
										errorMessage={
											errors.accountantEmail && touched.accountantEmail
												? errors.accountantEmail
												: ''
										}
										keyboardType="email-address"
									/>
									<FormikInput
										label="Invite your legal advisor"
										placeholder="name@example.com"
										containerStyle={commonStyles.inputContainer}
										name="advisorEmail"
										onChangeText={setFieldValue}
										value={values.advisorEmail}
										onBlur={setFieldTouched}
										errorMessage={
											errors.advisorEmail && touched.advisorEmail
												? errors.advisorEmail
												: ''
										}
										keyboardType="email-address"
									/>
									<Button
										title="Send Invitations"
										onPress={handleSubmit}
										containerStyle={commonStyles.formButtonContainer}
									/>
								</View>
							)}
						/>
					</KeyboardAvoidingView>
					<Text style={[human.footnote]}>
						Or, you can{' '}
						<Text style={{ fontWeight: 'bold' }}>get an invite link</Text> and
						share with other team members.
					</Text>
					<View style={commonStyles.rowContainer}>
						<Button
							title="Skip for now"
							onPress={this.skipHandler}
							containerStyle={commonStyles.formButtonContainer}
						/>
					</View>
				</View>
			</ScrollView>
		);
	}
}

export default Invite;
