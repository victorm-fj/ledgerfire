// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button, CheckBox } from 'react-native-elements';
import { human } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	firstName: validation.name,
	lastName: validation.name,
});

type Props = { navigation: NavigationScreenProp<*> };
type State = { checked: boolean };
class Name extends React.Component<Props, State> {
	state = { checked: true };
	onCheckHandler = () => {
		this.setState((prevState) => ({ checked: !prevState.checked }));
	};
	onSubmitHandler = ({
		firstName,
		lastName,
	}: {
		firstName: string,
		lastName: string,
	}) => {
		console.log(firstName, lastName);
		this.props.navigation.navigate('Password');
	};
	singInHandler = () => {};
	render() {
		return (
			<View style={[commonStyles.screenContainer]}>
				<Text style={[human.title1, { paddingBottom: 24 }]}>
					What&apos;s your name?
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ firstName: '', lastName: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<View>
									<FormikInput
										label="First name"
										placeholder="First name"
										containerStyle={commonStyles.inputContainer}
										name="firstName"
										onChangeText={setFieldValue}
										value={values.firstName}
										onBlur={setFieldTouched}
										errorMessage={
											errors.firstName && touched.firstName
												? errors.firstName
												: ''
										}
									/>
									<FormikInput
										label="Last name"
										placeholder="Last name"
										containerStyle={commonStyles.inputContainer}
										name="lastName"
										onChangeText={setFieldValue}
										value={values.lastName}
										onBlur={setFieldTouched}
										errorMessage={
											errors.lastName && touched.lastName ? errors.lastName : ''
										}
									/>
									<Text style={[human.subhead, { paddingVertical: 16 }]}>
										This is how all your business partners in Ledgerfire will
										see and refer to you.
									</Text>
								</View>
								<Button
									title="Continue to password"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
				<CheckBox
					title="It's ok to send me emails about the Ledgerfire service."
					checked={this.state.checked}
					onPress={this.onCheckHandler}
				/>
			</View>
		);
	}
}

export default Name;
