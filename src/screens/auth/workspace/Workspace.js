// @flow
import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';
import { NavigationScreenProp } from 'react-navigation';

import commonStyles from '../../commonStyles';

type Props = { navigation: NavigationScreenProp<*> };
class Workspace extends React.Component<Props> {
	createFinspaceHandler = () => {
		this.props.navigation.navigate('Name');
	};
	joinFinspaceHandler = () => {
		this.props.navigation.navigate('Name');
	};
	signInHandler = () => {};
	render() {
		return (
			<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
				<View style={commonStyles.screenContainer}>
					<Text style={[human.title2, { paddingBottom: 24 }]}>
						Please check your phone
					</Text>
					<Text style={[human.body, { paddingBottom: 16 }]}>
						In Ledgerfire everything happens in a{' '}
						<Text style={{ fontWeight: 'bold' }}>finspace</Text>. A finspace is
						simply your company&apos;s dedicated financial environment where you
						can collaborate with your business partners to optimize and grow
						your business.{' '}
						<Text style={{ fontWeight: 'bold' }}>
							Is your team already on Ledgerfire, or do you need to start a new
							finspace?
						</Text>
					</Text>
					<Card title="Join a finspace" containerStyle={{ width: '70%' }}>
						<Text style={[human.body, { paddingBottom: 16 }]}>
							Find and join or sign in to an existing company finspace.
						</Text>
						<Button
							title="Join a finspace"
							onPress={this.joinFinspaceHandler}
						/>
					</Card>
					<Card title="Create a finspace" containerStyle={{ width: '70%' }}>
						<Text style={[human.body, { paddingBottom: 16 }]}>
							Create a finspace and get your organization on Ledgerfire.
						</Text>
						<Button
							title="Create new finspace"
							onPress={this.createFinspaceHandler}
						/>
					</Card>
					<View style={commonStyles.footnoteContainer}>
						<Text
							style={[
								human.footnote,
								{ color: iOSColors.gray, paddingVertical: 5 },
							]}
						>
							Already using Ledgerfire?
						</Text>
						<Button
							title="Sign in"
							titleStyle={[human.footnote, commonStyles.footnoteText]}
							clear
							onPress={this.signInHandler}
						/>
					</View>
				</View>
			</ScrollView>
		);
	}
}

export default Workspace;
