import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { PasswordInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	password: validation.password,
	passwordConfirm: validation.passwordConfirm,
});

type Props = { navigation: NavigationScreenProp<*> };
class Password extends React.Component<Props> {
	onSubmitHandler = () => {
		this.props.navigation.navigate('CompanyName');
	};
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Set up your password for signing in
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ password: '', passwordConfirm: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<View>
									<PasswordInput
										setFieldValue={setFieldValue}
										value={values.password}
										setFieldTouched={setFieldTouched}
										error={errors.password}
										touched={touched.password}
									/>
									<PasswordInput
										placeholder="Confirm password"
										name="passwordConfirm"
										setFieldValue={setFieldValue}
										value={values.passwordConfirm}
										setFieldTouched={setFieldTouched}
										error={errors.passwordConfirm}
										touched={touched.passwordConfirm}
									/>
								</View>
								<Button
									title="Continue to finspace"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
				<Text
					style={[human.subhead, { color: iOSColors.gray, paddingVertical: 5 }]}
				>
					Password must contain numbers, at least one special character,
					uppercase and lowercase letters, and at least 8 characters long.
				</Text>
			</View>
		);
	}
}

export default Password;
