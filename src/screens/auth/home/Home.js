// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({ email: validation.email });

type Props = { navigation: NavigationScreenProp<*>, updateAuthInfo: Function };
class Home extends React.Component<Props> {
	onSubmitHandler = ({ email }: { email: string }) => {
		const { updateAuthInfo, navigation } = this.props;
		updateAuthInfo({ email });
		navigation.navigate('Phone');
	};
	singInHandler = () => {
		this.props.navigation.navigate('SignIn');
	};
	render() {
		return (
			<View style={[commonStyles.screenContainer]}>
				<Text style={[human.title1, { paddingBottom: 24 }]}>
					Where financial growth happens
				</Text>
				<Text style={[human.body, { paddingBottom: 16 }]}>
					When your teams needs to know the salaries you can draw based on
					personal need, understand the impact personal finances have on the
					business, plan investments and hires, create new KPIs, and more,
					Ledgerfire has you covered.
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ email: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									placeholder="Email"
									containerStyle={commonStyles.inputContainer}
									name="email"
									onChangeText={setFieldValue}
									value={values.email}
									onBlur={setFieldTouched}
									errorMessage={
										errors.email && touched.email ? errors.email : ''
									}
									keyboardType="email-address"
								/>
								<Button
									title="GET STARTED NOW"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
				<Text style={[human.footnote, { fontWeight: 'bold' }]}>
					Ledgerfire is free to use as long as you want and with unlimited team
					members
				</Text>
				<View style={commonStyles.footnoteContainer}>
					<Text
						style={[
							human.footnote,
							{ color: iOSColors.gray, paddingVertical: 5 },
						]}
					>
						Already using Ledgerfire?
					</Text>
					<Button
						title="Sign in"
						titleStyle={[human.footnote, commonStyles.footnoteText]}
						clear
						onPress={this.singInHandler}
					/>
				</View>
			</View>
		);
	}
}

export default Home;
