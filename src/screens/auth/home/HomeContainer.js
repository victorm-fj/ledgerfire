// @flow
import { compose, graphql } from 'react-apollo';

import { AuthInfoQuery, UpdateAuthInfo } from '../../../graphQL';
import Home from './Home';

export default compose(
	graphql(AuthInfoQuery, {
		props: ({
			data: {
				authInfo: { email },
			},
		}) => ({ email }),
	}),
	graphql(UpdateAuthInfo, {
		props: ({ mutate }) => ({
			updateAuthInfo: (info) => mutate({ variables: { info } }),
		}),
	})
)(Home);
