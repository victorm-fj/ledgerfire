// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	companyName: validation.companyName,
});

type Props = { navigation: NavigationScreenProp<*> };
class CompanyName extends React.Component<Props> {
	onSubmitHandler = () => {
		this.props.navigation.navigate('CompanyInfo');
	};
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Create your company&apos;s finspace
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ companyName: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									label="What's your company called?"
									placeholder="Company Name"
									containerStyle={commonStyles.inputContainer}
									name="companyName"
									onChangeText={setFieldValue}
									value={values.companyName}
									onBlur={setFieldTouched}
									errorMessage={
										errors.companyName && touched.companyName
											? errors.companyName
											: ''
									}
								/>
								<Button
									title="Create finspace"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
				<Text style={[human.subhead, { paddingVertical: 5 }]}>
					We&apos;ll use this to name your Ledgerfire finspace, which you can
					always change.
				</Text>
				<Text
					style={[
						human.footnote,
						{ color: iOSColors.gray, paddingVertical: 5 },
					]}
				>
					By clicking &quot;Create Finspace&quot;, you understand and agree to
					the Contract on behalf of the customer (as described in our{' '}
					<Text style={{ fontWeight: 'bold' }}>Customer Terms of Service</Text>),{' '}
					<Text style={{ fontWeight: 'bold' }}>Privacy Policy</Text> and{' '}
					<Text style={{ fontWeight: 'bold' }}>Cookie Policy</Text>.
				</Text>
			</View>
		);
	}
}

export default CompanyName;
