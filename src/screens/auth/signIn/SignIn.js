// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';

import { FormikInput, PasswordInput } from '../../../components/inputs';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	email: validation.email,
	password: validation.password,
});

type Props = { navigation: NavigationScreenProp<*> };
class Signin extends React.Component<Props> {
	onSubmitHandler = () => {
		this.props.navigation.navigate('ConfirmSignIn');
	};
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>Sign In</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ email: '', password: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<View>
									<FormikInput
										placeholder="Email"
										containerStyle={commonStyles.inputContainer}
										name="email"
										onChangeText={setFieldValue}
										value={values.email}
										onBlur={setFieldTouched}
										errorMessage={
											errors.email && touched.email ? errors.email : ''
										}
										keyboardType="email-address"
									/>
									<PasswordInput
										setFieldValue={setFieldValue}
										value={values.password}
										setFieldTouched={setFieldTouched}
										error={errors.password}
										touched={touched.password}
									/>
								</View>
								<Button
									title="Sign in"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

export default Signin;
