// @flow
import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { Button } from 'react-native-elements';
import { human } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { NavigationScreenProp } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';

import { PhoneInput } from '../../../components/inputs';
import { getCountry } from '../../../utils/countriesInfo';
import commonStyles, { platformBehavior } from '../../commonStyles';
import validation from '../../validation';

const validationSchema = Yup.object().shape({
	phoneNumber: validation.phoneNumber,
});

type Props = { navigation: NavigationScreenProp<*> };
type State = { country: Object };
class Phone extends React.Component<Props, State> {
	constructor(props: Object) {
		super(props);
		const userLocaleCountryCode = DeviceInfo.getDeviceCountry();
		const country = getCountry(userLocaleCountryCode);
		const callingCode = country ? country.dial : '44';
		const cca2 = userLocaleCountryCode || 'GB';
		this.state = { country: { callingCode, cca2 } };
	}
	onSubmitHandler = ({ phoneNumber }: { phoneNumber: string }) => {
		console.log('phoneNumber', phoneNumber);
		this.props.navigation.navigate('ConfirmPhone');
	};
	selectCountry = (country: Object) => {
		this.setState({ country });
	};
	render() {
		const {
			country: { cca2 },
		} = this.state;
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Please enter your phone number
				</Text>
				<Text style={[human.body, { paddingBottom: 16 }]}>
					A 6-digit confirmation code will be sent to your phone number via SMS.
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ phoneNumber: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({
							setFieldValue,
							values,
							setFieldTouched,
							errors,
							touched,
							handleSubmit,
						}) => (
							<View style={commonStyles.formContainer}>
								<PhoneInput
									label="Your phone number"
									placeholder="Phone number"
									setFieldValue={setFieldValue}
									value={values.phoneNumber}
									setFieldTouched={setFieldTouched}
									error={errors.phoneNumber}
									touched={touched.phoneNumber}
									selectCountry={this.selectCountry}
									cca2={cca2}
								/>
								<Button
									title="Continue"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

export default Phone;
