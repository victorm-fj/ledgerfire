// @flow
import React from 'react';
import { Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { human } from 'react-native-typography';
import { Formik } from 'formik';
import { NavigationScreenProp } from 'react-navigation';

import {
	businessStructureOptions,
	partnerShipOptions,
	numPartnersOptions,
	companyTypeOptions,
} from '../../../utils/companyInfo';
import { PickerInput } from '../../../components/inputs';
import commonStyles from '../../commonStyles';

type Props = { navigation: NavigationScreenProp<*> };
class CompanyInfo extends React.Component<Props> {
	onSubmitHandler = () => {
		this.props.navigation.navigate('Invite');
	};
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Tell me about your company
				</Text>
				<Formik
					initialValues={{
						businessStructure: 0,
						partnerShip: 0,
						numPartners: 0,
						companyType: 0,
					}}
					onSubmit={this.onSubmitHandler}
					render={({ setFieldValue, values, handleSubmit }) => (
						<View style={commonStyles.formContainer}>
							<PickerInput
								label="What's your business structure?"
								name="businessStructure"
								value={values.businessStructure}
								setValue={setFieldValue}
								options={businessStructureOptions}
							/>
							<PickerInput
								label="Are you the sole equity owner, or do you have partners?"
								name="partnerShip"
								value={values.partnerShip}
								setValue={setFieldValue}
								options={partnerShipOptions}
							/>
							<PickerInput
								label="How many partners that own over 10% do you have?"
								name="numPartners"
								value={values.numPartners}
								setValue={setFieldValue}
								options={numPartnersOptions}
							/>
							<PickerInput
								label="Great! What type of company is it?"
								name="companyType"
								value={values.companyType}
								setValue={setFieldValue}
								options={companyTypeOptions}
							/>
							<Button
								title="Done"
								onPress={handleSubmit}
								containerStyle={commonStyles.formButtonContainer}
								icon={{
									name: 'arrow-forward',
									size: 20,
									color: 'white',
								}}
								iconRight
							/>
						</View>
					)}
				/>
			</View>
		);
	}
}

export default CompanyInfo;
