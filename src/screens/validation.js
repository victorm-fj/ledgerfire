import * as Yup from 'yup';

export default {
	companyName: Yup.string()
		.min(3, 'Too short')
		.required('Required'),
	code: Yup.string()
		.matches(
			/[\d]{6}/,
			'This code isn\'t valid. Please copy and paste the code the SMS you received.'
		)
		.max(
			6,
			'This code isn\'t valid. Please copy and paste the code the SMS you received.'
		)
		.required('Code is required'),
	email: Yup.string()
		.email('Invalid email.')
		.required('Required.'),
	name: Yup.string()
		.min(3, 'Too short')
		.required('Required'),
	password: Yup.string()
		.matches(
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
			// 'Password must contain at least 8 characters, 1 number,
			// 1 upper and 1 lowercase letter, and 1 special character'
			'Invalid password.'
		)
		.required('Password is required'),
	passwordConfirm: Yup.string()
		.oneOf([Yup.ref('password')], 'Passwords do not match')
		.required('Password confirm is required'),
	phoneNumber: Yup.string()
		.min(5, 'Too short')
		.required('Required'),
};
