export default [
	[
		{
			x: 66.5,
			y: 213,
		},
		{
			x: 66.5,
			y: 213,
		},
		{
			x: 66.5,
			y: 213,
		},
		{
			x: 66.5,
			y: 213,
		},
		{
			x: 70.5,
			y: 213,
		},
		{
			x: 74,
			y: 213,
		},
		{
			x: 79.5,
			y: 213,
		},
		{
			x: 88,
			y: 213,
		},
		{
			x: 104,
			y: 213,
		},
		{
			x: 128.5,
			y: 212,
		},
		{
			x: 155,
			y: 209.5,
		},
		{
			x: 173.5,
			y: 208.5,
		},
		{
			x: 186,
			y: 207.5,
		},
		{
			x: 195,
			y: 207.5,
		},
		{
			x: 201,
			y: 207.5,
		},
		{
			x: 207,
			y: 207.5,
		},
		{
			x: 209.5,
			y: 207.5,
		},
		{
			x: 210,
			y: 207.5,
		},
		{
			x: 210,
			y: 207.5,
		},
		{
			x: 209,
			y: 207.5,
		},
		{
			x: 204,
			y: 207.5,
		},
		{
			x: 196,
			y: 207.5,
		},
		{
			x: 183.5,
			y: 208,
		},
		{
			x: 161.5,
			y: 213,
		},
		{
			x: 136,
			y: 221,
		},
		{
			x: 109.5,
			y: 231,
		},
		{
			x: 86.5,
			y: 240,
		},
		{
			x: 71,
			y: 244.5,
		},
		{
			x: 59.5,
			y: 248,
		},
		{
			x: 53,
			y: 249.5,
		},
		{
			x: 49.5,
			y: 250.5,
		},
		{
			x: 48,
			y: 251,
		},
		{
			x: 48,
			y: 251,
		},
		{
			x: 49,
			y: 251,
		},
		{
			x: 52.5,
			y: 251,
		},
		{
			x: 57,
			y: 251,
		},
		{
			x: 65.5,
			y: 251,
		},
		{
			x: 78.5,
			y: 251,
		},
		{
			x: 97,
			y: 250.5,
		},
		{
			x: 116,
			y: 249.5,
		},
		{
			x: 134,
			y: 248.5,
		},
		{
			x: 151,
			y: 248,
		},
		{
			x: 163.5,
			y: 248,
		},
		{
			x: 174,
			y: 248,
		},
		{
			x: 181.5,
			y: 248,
		},
		{
			x: 188.5,
			y: 248,
		},
		{
			x: 192.5,
			y: 248,
		},
		{
			x: 195,
			y: 248,
		},
		{
			x: 196.5,
			y: 248,
		},
		{
			x: 198,
			y: 248,
		},
		{
			x: 199,
			y: 248,
		},
		{
			x: 200.5,
			y: 248,
		},
		{
			x: 202,
			y: 248,
		},
		{
			x: 203,
			y: 248,
		},
		{
			x: 204,
			y: 248,
		},
		{
			x: 206,
			y: 248,
		},
		{
			x: 207.5,
			y: 248,
		},
		{
			x: 210,
			y: 248,
		},
		{
			x: 213,
			y: 248,
		},
		{
			x: 216.5,
			y: 248,
		},
		{
			x: 219.5,
			y: 248,
		},
		{
			x: 222.5,
			y: 248,
		},
		{
			x: 225.5,
			y: 248,
		},
		{
			x: 227,
			y: 248,
		},
		{
			x: 228.5,
			y: 248.5,
		},
		{
			x: 230,
			y: 248.5,
		},
		{
			x: 231,
			y: 248.5,
		},
		{
			x: 231.5,
			y: 249,
		},
		{
			x: 232,
			y: 249,
		},
		{
			x: 232.5,
			y: 249,
		},
		{
			x: 232.5,
			y: 249,
		},
		{
			x: 232.5,
			y: 249,
		},
	],
	[
		{
			x: 85,
			y: 199.5,
		},
		{
			x: 85,
			y: 199.5,
		},
		{
			x: 85,
			y: 199.5,
		},
		{
			x: 85,
			y: 199.5,
		},
		{
			x: 90,
			y: 200,
		},
		{
			x: 93.5,
			y: 200,
		},
		{
			x: 98.5,
			y: 200,
		},
		{
			x: 109,
			y: 200,
		},
		{
			x: 121,
			y: 200,
		},
		{
			x: 138,
			y: 200,
		},
		{
			x: 151,
			y: 200.5,
		},
		{
			x: 165,
			y: 201,
		},
		{
			x: 177.5,
			y: 201.5,
		},
		{
			x: 189.5,
			y: 202,
		},
		{
			x: 198,
			y: 202,
		},
		{
			x: 206,
			y: 202,
		},
		{
			x: 212,
			y: 202.5,
		},
		{
			x: 216,
			y: 202.5,
		},
		{
			x: 218.5,
			y: 202.5,
		},
		{
			x: 219,
			y: 202.5,
		},
		{
			x: 219,
			y: 203,
		},
		{
			x: 219,
			y: 203,
		},
		{
			x: 219,
			y: 203.5,
		},
		{
			x: 217.5,
			y: 204,
		},
		{
			x: 214.5,
			y: 204.5,
		},
		{
			x: 211.5,
			y: 205,
		},
		{
			x: 207,
			y: 206,
		},
		{
			x: 200.5,
			y: 207,
		},
		{
			x: 194,
			y: 208.5,
		},
		{
			x: 187,
			y: 210,
		},
		{
			x: 177.5,
			y: 212.5,
		},
		{
			x: 168.5,
			y: 215,
		},
		{
			x: 155.5,
			y: 219,
		},
		{
			x: 143,
			y: 222.5,
		},
		{
			x: 131,
			y: 227,
		},
		{
			x: 121,
			y: 229.5,
		},
		{
			x: 111,
			y: 233,
		},
		{
			x: 101.5,
			y: 236,
		},
		{
			x: 95.5,
			y: 238,
		},
		{
			x: 89.5,
			y: 240,
		},
		{
			x: 84.5,
			y: 241.5,
		},
		{
			x: 81,
			y: 243,
		},
		{
			x: 79,
			y: 244,
		},
		{
			x: 79,
			y: 245,
		},
		{
			x: 79,
			y: 245.5,
		},
		{
			x: 79.5,
			y: 246,
		},
		{
			x: 82.5,
			y: 246,
		},
		{
			x: 86.5,
			y: 246,
		},
		{
			x: 93,
			y: 246,
		},
		{
			x: 102,
			y: 246,
		},
		{
			x: 114.5,
			y: 246,
		},
		{
			x: 127.5,
			y: 246,
		},
		{
			x: 141,
			y: 246,
		},
		{
			x: 154.5,
			y: 245.5,
		},
		{
			x: 168,
			y: 244.5,
		},
		{
			x: 179,
			y: 244,
		},
		{
			x: 188,
			y: 244,
		},
		{
			x: 196.5,
			y: 244,
		},
		{
			x: 202.5,
			y: 244,
		},
		{
			x: 209,
			y: 244,
		},
		{
			x: 213,
			y: 244,
		},
		{
			x: 215.5,
			y: 244,
		},
		{
			x: 217,
			y: 244,
		},
		{
			x: 218,
			y: 244,
		},
		{
			x: 218.5,
			y: 244,
		},
		{
			x: 218.5,
			y: 244,
		},
		{
			x: 218.5,
			y: 244,
		},
		{
			x: 218.5,
			y: 244,
		},
		{
			x: 219,
			y: 244,
		},
		{
			x: 219.5,
			y: 244,
		},
		{
			x: 219.5,
			y: 244,
		},
		{
			x: 219.5,
			y: 243.5,
		},
		{
			x: 219.5,
			y: 243,
		},
		{
			x: 219.5,
			y: 241.5,
		},
	],
	[
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 67.5,
			y: 243,
		},
		{
			x: 92.5,
			y: 244.5,
		},
		{
			x: 119.5,
			y: 244.5,
		},
		{
			x: 151,
			y: 244,
		},
		{
			x: 182,
			y: 243.5,
		},
		{
			x: 208.5,
			y: 243,
		},
		{
			x: 227.5,
			y: 243,
		},
		{
			x: 241,
			y: 243,
		},
		{
			x: 251,
			y: 243,
		},
		{
			x: 258.5,
			y: 243,
		},
		{
			x: 265,
			y: 243,
		},
		{
			x: 269,
			y: 243,
		},
		{
			x: 271.5,
			y: 243,
		},
		{
			x: 272.5,
			y: 243.5,
		},
		{
			x: 272.5,
			y: 244,
		},
		{
			x: 272.5,
			y: 244.5,
		},
		{
			x: 272.5,
			y: 245,
		},
		{
			x: 270,
			y: 245.5,
		},
		{
			x: 266,
			y: 246,
		},
		{
			x: 260.5,
			y: 247,
		},
		{
			x: 251,
			y: 248,
		},
		{
			x: 235.5,
			y: 251,
		},
		{
			x: 206,
			y: 257,
		},
		{
			x: 163,
			y: 265.5,
		},
		{
			x: 138.5,
			y: 270.5,
		},
		{
			x: 122.5,
			y: 275,
		},
		{
			x: 109.5,
			y: 278.5,
		},
		{
			x: 98.5,
			y: 281.5,
		},
		{
			x: 91.5,
			y: 284,
		},
		{
			x: 85.5,
			y: 286,
		},
		{
			x: 82.5,
			y: 288,
		},
		{
			x: 79.5,
			y: 289.5,
		},
		{
			x: 77.5,
			y: 290.5,
		},
		{
			x: 76.5,
			y: 291.5,
		},
		{
			x: 76,
			y: 292.5,
		},
		{
			x: 76,
			y: 293,
		},
		{
			x: 76,
			y: 293.5,
		},
		{
			x: 76,
			y: 294,
		},
		{
			x: 79,
			y: 294,
		},
		{
			x: 86,
			y: 294,
		},
		{
			x: 102,
			y: 294.5,
		},
		{
			x: 127.5,
			y: 294.5,
		},
		{
			x: 154.5,
			y: 294.5,
		},
		{
			x: 181,
			y: 294.5,
		},
		{
			x: 205,
			y: 292.5,
		},
		{
			x: 221.5,
			y: 291.5,
		},
		{
			x: 233.5,
			y: 291,
		},
		{
			x: 243.5,
			y: 291,
		},
		{
			x: 251,
			y: 290.5,
		},
		{
			x: 257,
			y: 290,
		},
		{
			x: 260.5,
			y: 290,
		},
		{
			x: 262,
			y: 290,
		},
		{
			x: 263.5,
			y: 290,
		},
		{
			x: 264,
			y: 290,
		},
		{
			x: 264,
			y: 290,
		},
		{
			x: 264,
			y: 290,
		},
		{
			x: 262,
			y: 290,
		},
		{
			x: 260.5,
			y: 290,
		},
		{
			x: 259.5,
			y: 290,
		},
		{
			x: 259.5,
			y: 290,
		},
	],
	[
		{
			x: 69,
			y: 330,
		},
		{
			x: 69,
			y: 330,
		},
		{
			x: 69,
			y: 330,
		},
		{
			x: 69,
			y: 330,
		},
		{
			x: 69,
			y: 330,
		},
		{
			x: 69,
			y: 330,
		},
		{
			x: 90,
			y: 326,
		},
		{
			x: 103,
			y: 326,
		},
		{
			x: 122,
			y: 324.5,
		},
		{
			x: 147,
			y: 321.5,
		},
		{
			x: 169.5,
			y: 317.5,
		},
		{
			x: 188,
			y: 314.5,
		},
		{
			x: 207,
			y: 311,
		},
		{
			x: 220,
			y: 308.5,
		},
		{
			x: 231,
			y: 307,
		},
		{
			x: 237.5,
			y: 306,
		},
		{
			x: 241.5,
			y: 306,
		},
		{
			x: 242.5,
			y: 306,
		},
		{
			x: 242,
			y: 306,
		},
		{
			x: 237.5,
			y: 306.5,
		},
		{
			x: 229.5,
			y: 309.5,
		},
		{
			x: 212,
			y: 315.5,
		},
		{
			x: 189.5,
			y: 322,
		},
		{
			x: 167,
			y: 328.5,
		},
		{
			x: 145.5,
			y: 335.5,
		},
		{
			x: 128,
			y: 343,
		},
		{
			x: 111,
			y: 349.5,
		},
		{
			x: 99.5,
			y: 354,
		},
		{
			x: 91.5,
			y: 358,
		},
		{
			x: 85,
			y: 361.5,
		},
		{
			x: 79.5,
			y: 365,
		},
		{
			x: 75,
			y: 368.5,
		},
		{
			x: 71.5,
			y: 371,
		},
		{
			x: 70.5,
			y: 372.5,
		},
		{
			x: 70,
			y: 373,
		},
		{
			x: 70,
			y: 373.5,
		},
		{
			x: 76,
			y: 373.5,
		},
		{
			x: 86.5,
			y: 373.5,
		},
		{
			x: 99.5,
			y: 373.5,
		},
		{
			x: 118,
			y: 372,
		},
		{
			x: 136.5,
			y: 369.5,
		},
		{
			x: 156,
			y: 366,
		},
		{
			x: 177,
			y: 363,
		},
		{
			x: 198,
			y: 361,
		},
		{
			x: 218.5,
			y: 358.5,
		},
		{
			x: 236,
			y: 357,
		},
		{
			x: 247.5,
			y: 357,
		},
		{
			x: 254.5,
			y: 356.5,
		},
		{
			x: 258,
			y: 356.5,
		},
		{
			x: 260,
			y: 356.5,
		},
		{
			x: 260.5,
			y: 356,
		},
		{
			x: 259,
			y: 356,
		},
		{
			x: 259,
			y: 356,
		},
	],
	[
		{
			x: 138,
			y: 217.5,
		},
		{
			x: 138,
			y: 217.5,
		},
		{
			x: 138,
			y: 217.5,
		},
		{
			x: 141.5,
			y: 217,
		},
		{
			x: 144.5,
			y: 217,
		},
		{
			x: 148,
			y: 217,
		},
		{
			x: 152,
			y: 217,
		},
		{
			x: 158,
			y: 217,
		},
		{
			x: 169.5,
			y: 217,
		},
		{
			x: 181.5,
			y: 217,
		},
		{
			x: 194.5,
			y: 216.5,
		},
		{
			x: 208.5,
			y: 216,
		},
		{
			x: 220.5,
			y: 215,
		},
		{
			x: 230.5,
			y: 214,
		},
		{
			x: 239.5,
			y: 214,
		},
		{
			x: 246,
			y: 214,
		},
		{
			x: 250,
			y: 214,
		},
		{
			x: 253.5,
			y: 214,
		},
		{
			x: 255,
			y: 214,
		},
		{
			x: 255.5,
			y: 214,
		},
		{
			x: 255.5,
			y: 214,
		},
		{
			x: 254.5,
			y: 214,
		},
		{
			x: 252,
			y: 214.5,
		},
		{
			x: 248.5,
			y: 215,
		},
		{
			x: 245,
			y: 215.5,
		},
		{
			x: 241,
			y: 217,
		},
		{
			x: 236,
			y: 218.5,
		},
		{
			x: 230.5,
			y: 220,
		},
		{
			x: 222,
			y: 222.5,
		},
		{
			x: 212,
			y: 224.5,
		},
		{
			x: 201,
			y: 228,
		},
		{
			x: 190,
			y: 231,
		},
		{
			x: 178,
			y: 234,
		},
		{
			x: 169,
			y: 237,
		},
		{
			x: 161,
			y: 239,
		},
		{
			x: 154.5,
			y: 242,
		},
		{
			x: 149,
			y: 244.5,
		},
		{
			x: 144,
			y: 246,
		},
		{
			x: 140.5,
			y: 247.5,
		},
		{
			x: 138,
			y: 249,
		},
		{
			x: 136.5,
			y: 250,
		},
		{
			x: 135.5,
			y: 250.5,
		},
		{
			x: 135.5,
			y: 251,
		},
		{
			x: 135.5,
			y: 251.5,
		},
		{
			x: 136.5,
			y: 251.5,
		},
		{
			x: 140.5,
			y: 251.5,
		},
		{
			x: 145.5,
			y: 251.5,
		},
		{
			x: 153,
			y: 251.5,
		},
		{
			x: 161.5,
			y: 251.5,
		},
		{
			x: 173,
			y: 251.5,
		},
		{
			x: 184.5,
			y: 251.5,
		},
		{
			x: 196.5,
			y: 251.5,
		},
		{
			x: 208.5,
			y: 251.5,
		},
		{
			x: 217,
			y: 251,
		},
		{
			x: 225,
			y: 251,
		},
		{
			x: 233,
			y: 251,
		},
		{
			x: 239.5,
			y: 251,
		},
		{
			x: 245.5,
			y: 251,
		},
		{
			x: 250,
			y: 251,
		},
		{
			x: 253,
			y: 251,
		},
		{
			x: 256,
			y: 251,
		},
		{
			x: 257.5,
			y: 251,
		},
		{
			x: 259,
			y: 251,
		},
		{
			x: 260,
			y: 251,
		},
		{
			x: 261,
			y: 251,
		},
		{
			x: 261.5,
			y: 251,
		},
		{
			x: 263,
			y: 251,
		},
		{
			x: 263.5,
			y: 251,
		},
		{
			x: 264,
			y: 251,
		},
		{
			x: 264.5,
			y: 251,
		},
		{
			x: 264.5,
			y: 251,
		},
		{
			x: 264.5,
			y: 250,
		},
		{
			x: 264.5,
			y: 249.5,
		},
	],
	[
		{
			x: 83,
			y: 448.5,
		},
		{
			x: 83,
			y: 448.5,
		},
		{
			x: 83,
			y: 448.5,
		},
		{
			x: 83,
			y: 448.5,
		},
		{
			x: 83,
			y: 448.5,
		},
		{
			x: 90,
			y: 449,
		},
		{
			x: 96.5,
			y: 449,
		},
		{
			x: 108.5,
			y: 449,
		},
		{
			x: 126,
			y: 449,
		},
		{
			x: 150.5,
			y: 448.5,
		},
		{
			x: 176,
			y: 445,
		},
		{
			x: 203,
			y: 442,
		},
		{
			x: 224,
			y: 440.5,
		},
		{
			x: 242,
			y: 439.5,
		},
		{
			x: 255,
			y: 439.5,
		},
		{
			x: 265,
			y: 439.5,
		},
		{
			x: 271.5,
			y: 439,
		},
		{
			x: 275.5,
			y: 439,
		},
		{
			x: 276.5,
			y: 439,
		},
		{
			x: 276.5,
			y: 439,
		},
		{
			x: 276.5,
			y: 439,
		},
		{
			x: 275.5,
			y: 439.5,
		},
		{
			x: 272,
			y: 440,
		},
		{
			x: 266.5,
			y: 442,
		},
		{
			x: 252,
			y: 445.5,
		},
		{
			x: 225,
			y: 451.5,
		},
		{
			x: 198.5,
			y: 458,
		},
		{
			x: 179.5,
			y: 464,
		},
		{
			x: 159.5,
			y: 472.5,
		},
		{
			x: 140.5,
			y: 480.5,
		},
		{
			x: 123,
			y: 487.5,
		},
		{
			x: 109.5,
			y: 492,
		},
		{
			x: 97,
			y: 496.5,
		},
		{
			x: 88.5,
			y: 499,
		},
		{
			x: 81.5,
			y: 501,
		},
		{
			x: 77,
			y: 502.5,
		},
		{
			x: 75.5,
			y: 503,
		},
		{
			x: 75,
			y: 503.5,
		},
		{
			x: 76,
			y: 503.5,
		},
		{
			x: 81.5,
			y: 504,
		},
		{
			x: 90.5,
			y: 504,
		},
		{
			x: 103.5,
			y: 504,
		},
		{
			x: 127,
			y: 504,
		},
		{
			x: 152.5,
			y: 504,
		},
		{
			x: 178,
			y: 504,
		},
		{
			x: 201,
			y: 504,
		},
		{
			x: 220,
			y: 504,
		},
		{
			x: 237.5,
			y: 503.5,
		},
		{
			x: 250,
			y: 503,
		},
		{
			x: 260,
			y: 503,
		},
		{
			x: 270.5,
			y: 503,
		},
		{
			x: 277,
			y: 503,
		},
		{
			x: 281.5,
			y: 503,
		},
		{
			x: 284.5,
			y: 503,
		},
		{
			x: 285,
			y: 503,
		},
		{
			x: 285.5,
			y: 503,
		},
		{
			x: 285.5,
			y: 503,
		},
		{
			x: 285.5,
			y: 502.5,
		},
	],
];
