// @flow
import React from 'react';
import { View, Text } from 'react-native';
import { human } from 'react-native-typography';

import commonStyles from '../commonStyles';

type Props = {};
class Dashboard extends React.Component<Props> {
	render() {
		return (
			<View style={commonStyles.screenContainer}>
				<Text style={[human.title2, { paddingBottom: 24 }]}>
					Dashboard screen
				</Text>
			</View>
		);
	}
}

export default Dashboard;
