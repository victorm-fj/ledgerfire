import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { withClientState } from 'apollo-link-state';
import AWSAppSyncClient, {
	createAppSyncLink,
	createLinkWithCache,
} from 'aws-appsync';
import { Auth } from 'aws-amplify';
import merge from 'lodash.merge';

import { authInfo } from './resolvers';
import AppSync from './AppSync';
import typeDefs from './typeDefs';

const onErrorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors) {
		graphQLErrors.map(({ message, locations, path }) =>
			console.log(`[GraphQL error]: Message ${message}, Location: ${locations}, Path: ${path}`));
	}
	if (networkError) {
		console.log(`[Network error]: ${networkError}`);
	}
});
// State link (local data)
const stateLink = createLinkWithCache((cache) =>
	withClientState({
		cache,
		...merge(authInfo),
		typeDefs,
	}));
const appSyncLink = createAppSyncLink({
	url: AppSync.graphqlEndpoint,
	region: AppSync.region,
	auth: {
		type: AppSync.authenticationType,
		// IAM
		credentials: () => Auth.currentCredentials(),
	},
	complexObjectsCredentials: () => Auth.currentCredentials(),
	// disableOffline: true,
});
const link = ApolloLink.from([onErrorLink, stateLink, appSyncLink]);
const client = new AWSAppSyncClient({}, { link });

export default client;
