// // @flow
// import React from 'react';
// import { YellowBox } from 'react-native';
// import Amplify from 'aws-amplify';
// import { Rehydrated } from 'aws-appsync-react';
// import { ApolloProvider } from 'react-apollo';

// import awsmobile from './aws-exports';
// import Nav from './nav/Nav';
// import client from './client';

// // Amplify init
// Amplify.configure(awsmobile);

// type Props = {};
// class App extends React.Component<Props> {
// 	async componentWillMount() {
// 		client.initQueryManager();
// 		await client.resetStore();
// 	}
// 	render() {
// 		return (
// 			<ApolloProvider client={client}>
// 				<Rehydrated>
// 					<Nav />
// 				</Rehydrated>
// 			</ApolloProvider>
// 		);
// 	}
// }

// export default App;

/**
 * @format
 * @flow
 */
import React from 'react';
import { YellowBox } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import Nav from './nav/Nav';

type Props = {};
class App extends React.Component<Props> {
	componentDidMount() {
		SplashScreen.hide();
	}

	render() {
		return <Nav />;
	}
}

export default App;

YellowBox.ignoreWarnings([
	'Warning: isMounted(...) is deprecated', // in plain JavaScript React classes.
	'Module RCTImageLoader requires', // main queue setup since it overrides `init`
	'Class RCTCxxModule was not exported.', // Did you forget to use RCT_EXPORT_MODULE()
]);
