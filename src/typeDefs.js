export default `
  type AuthInfo {
    email: String
		phoneNumber: String
		firstName: String
		lastName: String
		emailSubscription: Boolean
		companyName: String
		businessStructure: Int
		partnerShip: Int
		numPartners: Int
		companyType: Int
  }
  type Query {
    authInfo: AuthInfo
  }
  type Mutation {
    updateAuthInfo(info: AuthInfo): AuthInfo
  }
`;
