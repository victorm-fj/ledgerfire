﻿export default {
	graphqlEndpoint:
		'https://ukdfr3k2ffhnzcw6wrnesfj74u.appsync-api.us-east-1.amazonaws.com/graphql',
	region: 'us-east-1',
	authenticationType: 'AWS_IAM',
	apiKey: 'null',
};
