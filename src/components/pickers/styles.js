import { StyleSheet } from 'react-native';
import { iOSColors } from 'react-native-typography';

export default StyleSheet.create({
	container: {
		flex: 1,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		zIndex: 1000,
	},
	overlay: {
		flex: 1.7,
		backgroundColor: 'rgba(0,0,0,0.3)',
	},
	pickerContainer: {
		flex: 1.3,
		backgroundColor: iOSColors.white,
	},
	buttonContainer: {
		padding: 20,
		borderTopWidth: 1,
		borderColor: iOSColors.gray,
	},
	buttonTitle: {
		color: iOSColors.blue,
	},
});
