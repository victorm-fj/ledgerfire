// @flow
import React from 'react';
import { View, Picker } from 'react-native';
import { Button } from 'react-native-elements';

import styles from './styles';

type Option = { id: number, label: string, value: string };
type Props = {
	options: Array<Option>,
	setSelectedValue: (selectedValue: string, selectedIndex: number) => void,
};
type State = {
	selectedValue: string,
	selectedIndex: number,
};
class SimplePicker extends React.Component<Props, State> {
	state = { selectedValue: this.props.options[0].value, selectedIndex: 0 };
	onValueChangeHandler = (selectedValue: string, selectedIndex: number) => {
		this.setState({ selectedValue, selectedIndex });
	};
	onPressDone = () => {
		const { selectedValue, selectedIndex } = this.state;
		this.props.setSelectedValue(selectedValue, selectedIndex);
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.overlay} />
				<View style={styles.pickerContainer}>
					<Picker
						selectedValue={this.state.selectedValue}
						onValueChange={this.onValueChangeHandler}
					>
						{this.props.options.map((option) => (
							<Picker.Item
								key={option.id}
								label={option.label}
								value={option.value}
							/>
						))}
					</Picker>
					<Button
						title="Done"
						clear
						conatinerStyle={styles.buttonContainer}
						titleStyle={styles.buttonTitle}
						onPress={this.onPressDone}
					/>
				</View>
			</View>
		);
	}
}

export default SimplePicker;
