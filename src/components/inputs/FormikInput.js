// @flow
import React from 'react';
import { Input } from 'react-native-elements';

type Props = {
	onChangeText: Function,
	onBlur: Function,
	name: string,
	label?: string,
};
class FormikInput extends React.Component<Props> {
	static defaultProps = { label: '' };
	handleChange = (value: string) => {
		const { onChangeText, name } = this.props;
		onChangeText(name, value);
	};
	handleBlur = () => {
		const { onBlur, name } = this.props;
		onBlur(name, true);
	};
	render() {
		const {
			onChangeText, onBlur, label, ...rest
		} = this.props;
		return (
			<Input
				label={label}
				onChangeText={this.handleChange}
				onBlur={this.handleBlur}
				{...rest}
			/>
		);
	}
}

export default FormikInput;
