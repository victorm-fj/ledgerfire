// @flow
import * as React from 'react';
import { TouchableOpacity, Modal, View } from 'react-native';
import { Input } from 'react-native-elements';

import { SimplePicker } from '../pickers';

type Option = { id: number, label: string, value: string };
type Props = {
	setValue: Function,
	name: string,
	label?: string,
	value: number,
	options: Array<Option>,
};
type State = {
	active: boolean,
};
class PickerInput extends React.Component<Props, State> {
	static defaultProps = { label: '' };
	state = { active: false };
	setSelectedValue = (selectedValue: string, selectedIndex: number) => {
		this.props.setValue(this.props.name, selectedIndex);
		this.togglePicker();
	};
	togglePicker = () => {
		this.setState((prevState) => ({ active: !prevState.active }));
	};
	render() {
		const { active } = this.state;
		const {
			setValue, value, label, options, ...rest
		} = this.props;
		return (
			<React.Fragment>
				<Modal visible={active} animationType="fade" transparent>
					<SimplePicker
						setSelectedValue={this.setSelectedValue}
						options={this.props.options}
					/>
				</Modal>
				<TouchableOpacity
					onPress={() => this.setState({ active: true })}
					style={{ width: '100%' }}
				>
					<View pointerEvents="none">
						<Input
							label={label}
							value={options[value].label}
							editable={false}
							{...rest}
						/>
					</View>
				</TouchableOpacity>
			</React.Fragment>
		);
	}
}

export default PickerInput;
