// @flow
import React from 'react';
import { ListItem, Icon } from 'react-native-elements';
import { iOSColors } from 'react-native-typography';

import FormikInput from './FormikInput';
import styles from './styles';

type Props = {
	placeholder: string,
	name: string,
	setFieldValue: Function,
	value: string,
	setFieldTouched: Function,
	error: string,
	touched: boolean,
};
type State = {
	secure: boolean,
};
class PasswordInput extends React.Component<Props, State> {
	static defaultProps = { placeholder: 'Password', name: 'password' };
	state = { secure: true };
	toggleSecurity = () =>
		this.setState((prevState) => ({ secure: !prevState.secure }));
	render() {
		const {
			placeholder,
			name,
			setFieldValue,
			value,
			setFieldTouched,
			error,
			touched,
		} = this.props;
		return (
			<ListItem
				containerStyle={styles.phoneInputContainer}
				title={
					<FormikInput
						placeholder={placeholder}
						containerStyle={styles.inputContainer}
						name={name}
						onChangeText={setFieldValue}
						value={value}
						onBlur={setFieldTouched}
						errorMessage={error && touched ? error : ''}
						secureTextEntry={this.state.secure}
					/>
				}
				rightIcon={
					<Icon
						type="ionicon"
						size={28}
						color={iOSColors.black}
						name={this.state.secure ? 'ios-eye' : 'ios-eye-off'}
						onPress={this.toggleSecurity}
					/>
				}
			/>
		);
	}
}

export default PasswordInput;
