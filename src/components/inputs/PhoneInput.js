// @flow
import React from 'react';
import { ListItem } from 'react-native-elements';
import CountryPicker from 'react-native-country-picker-modal';

import FormikInput from './FormikInput';
import styles from './styles';

type Props = {
	label: string,
	placeholder: string,
	name: string,
	setFieldValue: Function,
	value: string,
	setFieldTouched: Function,
	error: string,
	touched: boolean,
	selectCountry: Function,
	cca2: string,
};
const PhoneInput = (props: Props) => {
	const {
		label,
		placeholder,
		name,
		setFieldValue,
		value,
		setFieldTouched,
		error,
		touched,
		selectCountry,
		cca2,
	} = props;
	return (
		<ListItem
			containerStyle={styles.phoneInputContainer}
			leftIcon={
				<CountryPicker
					onChange={selectCountry}
					cca2={cca2}
					onClose={() => {}}
					filterable
					closeable
					filterPlaceholder="Type here"
					transparent
				/>
			}
			title={
				<FormikInput
					label={label}
					placeholder={placeholder}
					containerStyle={styles.inputContainer}
					name={name}
					onChangeText={setFieldValue}
					value={value}
					onBlur={setFieldTouched}
					errorMessage={error && touched ? error : ''}
					keyboardType="numeric"
				/>
			}
		/>
	);
};
PhoneInput.defaultProps = {
	label: '',
	placeholder: 'Phone number',
	name: 'phoneNumber',
};

export default PhoneInput;
