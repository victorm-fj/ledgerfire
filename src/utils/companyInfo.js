export const businessStructureOptions = [
	{ id: 0, label: 'Sole Proprietorship', value: 'Sole Proprietorship' },
	{
		id: 1,
		label: 'Limited Liability Company',
		value: 'Limited Liability Company',
	},
	{ id: 2, label: 'S Corporation', value: 'S Corporation' },
	{ id: 3, label: 'C Corporation', value: 'C Corporation' },
	{ id: 4, label: 'Partnership', value: 'Partnership' },
	{ id: 5, label: 'Cooperative', value: 'Cooperative' },
	{ id: 6, label: 'Unincorporated', value: 'Unincorporated' },
];

export const partnerShipOptions = [
	{ id: 0, label: 'Yes, I\'m the only owner', value: 'No' },
	{ id: 1, label: 'I have other co-owners', value: 'Yes' },
];

export const numPartnersOptions = [
	{ id: 0, label: '1', value: 0 },
	{ id: 1, label: '2', value: 1 },
	{ id: 2, label: '3', value: 2 },
	{ id: 3, label: '4', value: 3 },
	{ id: 4, label: '5', value: 4 },
	{ id: 5, label: '6', value: 5 },
	{ id: 5, label: '6', value: 6 },
];

export const companyTypeOptions = [
	{ id: 0, label: 'Travel Agency', value: 'Travel Agency' },
];
