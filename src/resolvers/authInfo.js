const authInfo = {
	defaults: {
		authInfo: {
			__typename: 'AuthInfo',
			email: '',
			phoneNumber: '',
			firstName: '',
			lastName: '',
			emailSubscription: true,
			companyName: '',
			businessStructure: 0,
			partnerShip: 0,
			numPartners: 0,
			companyType: 0,
		},
	},
	resolvers: {
		Mutation: {
			updateAuthInfo: (_, { info }, { cache }) => {
				const data = { authInfo: { __typename: 'AuthInfo', ...info } };
				cache.writeData({ data });
				return null;
			},
		},
	},
};

export default authInfo;
