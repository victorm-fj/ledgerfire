import gql from 'graphql-tag';

export default gql`
	query AuthInfoQuery {
		authInfo @client {
			__typename
			email
			phoneNumber
			firstName
			lastName
			emailSubscription
			companyName
			businessStructure
			partnerShip
			numPartners
			companyType
		}
	}
`;
