// import { createSwitchNavigator } from 'react-navigation';

// import AuthLoading from './AuthLoading';
// import AuthNav from './AuthNav';
// import AppNav from './AppNav';

// export default createSwitchNavigator(
// 	{
// 		Loading: AuthLoading,
// 		Auth: AuthNav,
// 		App: AppNav,
// 	},
// 	{
// 		initialRouteName: 'Loading',
// 	}
// );

import { createStackNavigator } from 'react-navigation';

import Intro from '../screens/intro/Intro';
import Canvas from '../screens/canvas/SketchCanvas';

export default createStackNavigator({
	Intro: { screen: Intro, navigationOptions: { header: null } },
	Canvas: { screen: Canvas, navigationOptions: { header: null } },
});

