// @flow
import { createStackNavigator } from 'react-navigation';

import Dashboard from '../screens/dashboard/Dashboard';

export default createStackNavigator({
	Dashboard: {
		screen: Dashboard,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
});
