// @flow
import React from 'react';
import { Auth } from 'aws-amplify';
import { NavigationScreenProp } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';

type Props = { navigation: NavigationScreenProp<*> };
class AuthLoading extends React.Component<Props> {
	componentDidMount() {
		this.checkUser();
	}
	async checkUser() {
		const { navigation } = this.props;
		try {
			await Auth.currentAuthenticatedUser();
			navigation.navigate('App');
		} catch (error) {
			console.log('AuthLoading error,', error);
			navigation.navigate('Auth');
		} finally {
			SplashScreen.hide();
		}
	}
	render() {
		return null;
	}
}

export default AuthLoading;
