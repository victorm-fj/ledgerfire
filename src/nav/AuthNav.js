// @flow
import { createStackNavigator } from 'react-navigation';

import HomeContainer from '../screens/auth/home/HomeContainer';
import Phone from '../screens/auth/phone/Phone';
import ConfirmPhone from '../screens/auth/confirmPhone/ConfirmPhone';
import ConfirmEmail from '../screens/auth/confirmEmail/ConfirmEmail';
import Workspace from '../screens/auth/workspace/Workspace';
import Name from '../screens/auth/name/Name';
import Password from '../screens/auth/password/Password';
import CompanyName from '../screens/auth/companyName/CompanyName';
import CompanyInfo from '../screens/auth/companyInfo/CompanyInfo';
import Invite from '../screens/auth/invite/Invite';
import SignIn from '../screens/auth/signIn/SignIn';
import ConfirmSignIn from '../screens/auth/signIn/ConfirmSignIn';

export default createStackNavigator({
	Home: {
		screen: HomeContainer,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	Phone: {
		screen: Phone,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	ConfirmPhone: {
		screen: ConfirmPhone,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	ConfirmEmail: {
		screen: ConfirmEmail,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	Workspace: {
		screen: Workspace,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	Name: {
		screen: Name,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	Password: {
		screen: Password,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	CompanyName: {
		screen: CompanyName,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	CompanyInfo: {
		screen: CompanyInfo,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	Invite: {
		screen: Invite,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	SignIn: {
		screen: SignIn,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
	ConfirmSignIn: {
		screen: ConfirmSignIn,
		navigationOptions: { title: 'Ledgerfire', headerBackTitle: 'Back' },
	},
});
